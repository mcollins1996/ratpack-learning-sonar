# noinspection SqlWithoutWhereForFile
# noinspection SqlResolveForFile
# noinspection SqlNoDataSourceInspectionForFile

DELETE
FROM project;

INSERT INTO project (id, external_id, created_on, updated_on)
VALUES (1, 'PROD', '2019-08-16 15:50:40.622', '2019-08-16 15:50:46.748');
