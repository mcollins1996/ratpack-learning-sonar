package real.al.knowles.ratpack.learning.project;

import org.junit.jupiter.api.*;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import ratpack.exec.ExecResult;
import ratpack.exec.Promise;
import ratpack.func.Factory;
import ratpack.test.exec.ExecHarness;
import real.al.knowles.ratpack.learning.database.DatabaseExecutor;

import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.verifyNoMoreInteractions;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
public class ProjectServiceTest {

    private static ExecHarness HARNESS;

    @Mock
    private DatabaseExecutor databaseExecutor;

    @Mock
    private ProjectRepository projectRepository;

    ProjectService projectService;


    @BeforeAll
    static void setUpClass() {
        HARNESS = ExecHarness.harness();
    }

    @AfterAll
    static void tearDownClass() {
        HARNESS.close();
    }

    @BeforeEach
    void setUp() {
        projectService = new ProjectService(projectRepository, databaseExecutor);
    }

    @AfterEach
    void tearDown() {
        verifyNoMoreInteractions(projectRepository);
    }

    @Test
    void createProject() throws Exception {
        ProjectRepresentation input = new ProjectRepresentation();
        input.setId(1231L);
        input.setExternalId("KEY");

        ProjectRepresentation output = new ProjectRepresentation();
        output.setId(1231L);
        output.setExternalId("KEY");
        output.setId(1L);

        when(projectRepository.createProject(input))
                .thenReturn(1L);

        when(databaseExecutor.executeWrite(any(Factory.class)))
                .thenAnswer(invocation ->
                        Promise.sync(invocation.getArgument(0, Factory.class)));

        ExecResult<ProjectRepresentation> result =
                HARNESS.yield(execution ->
                        projectService.createProject(input));

        assertThat(result.getValue()).isEqualTo(output);
    }


    @Test
    void getProject() throws Exception {
        ProjectRepresentation output = new ProjectRepresentation();
        output.setId(1231L);
        output.setExternalId("KEY");
        output.setId(1L);

        when(projectRepository.getProject(1L))
                .thenReturn(Optional.of(output));

        when(databaseExecutor.executeRead(any(Factory.class)))
                .thenAnswer(invocation ->
                        Promise.sync(invocation.getArgument(0, Factory.class)));

        ExecResult<ProjectRepresentation> result =
                HARNESS.yield(execution ->
                        projectService.getProject(1L));

        assertThat(result.getValue()).isEqualTo(output);
    }

}
