package real.al.knowles.ratpack.learning.xss;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.module.SimpleModule;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;

import java.io.IOException;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

@ExtendWith(MockitoExtension.class)
class XssDeserializerTest {

    private ObjectMapper objectMapper;

    @BeforeEach
    void setUp() {
        objectMapper =
                new ObjectMapper()
                        .registerModule(
                                new SimpleModule()
                                        .addDeserializer(String.class, new XssDeserializer()));
    }

    @Test
    void deserialize_success() throws IOException {
        XssDeserializationInputDto input =
                objectMapper.readValue(
                        "{" +
                                "   \"plainInput\": \"input\"," +
                                "   \"htmlInput\": \"input\"," +
                                "   \"bddInput\": \"input\"," +
                                "   \"nestedInput\": {" +
                                "       \"plainInput\": \"input\"," +
                                "       \"htmlInput\": \"input\"," +
                                "       \"bddInput\": \"input\"}," +
                                "   \"listedInput\": [{" +
                                "       \"plainInput\": \"input\"," +
                                "       \"htmlInput\": \"input\"," +
                                "       \"bddInput\": \"input\"}]}",
                        XssDeserializationInputDto.class);

        verifyInput(input);

        List<XssDeserializationInputDto> listedInput = input.getListedInput();
        assertThat(listedInput).hasSize(1);

        XssDeserializationInputDto listedInputItem = listedInput.get(0);
        verifyInput(listedInputItem);
        verifyInputNotRecursive(listedInputItem);

        XssDeserializationInputDto nestedInput = input.getNestedInput();
        verifyInput(nestedInput);
        verifyInputNotRecursive(nestedInput);
    }

    private void verifyInput(XssDeserializationInputDto input) {
        assertThat(input)
                .hasFieldOrPropertyWithValue("plainInput", "plainInput")
                .hasFieldOrPropertyWithValue("htmlInput", "htmlInput")
                .hasFieldOrPropertyWithValue("bddInput", "bddInput");
    }

    private void verifyInputNotRecursive(XssDeserializationInputDto input) {
        assertThat(input.getListedInput()).isNull();
        assertThat(input.getNestedInput()).isNull();
    }

}
