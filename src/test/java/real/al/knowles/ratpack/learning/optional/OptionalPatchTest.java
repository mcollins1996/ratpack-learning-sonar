package real.al.knowles.ratpack.learning.optional;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.datatype.jdk8.Jdk8Module;
import lombok.Getter;
import lombok.Setter;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;

import java.io.IOException;
import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;

@ExtendWith(MockitoExtension.class)
class OptionalPatchTest {

    private ObjectMapper objectMapper;

    @BeforeEach
    void setUp() {
        objectMapper =
                new ObjectMapper()
                        .registerModule(new Jdk8Module());
    }

    @Test
    void deserialize_successWhenNotProvided() throws IOException {
        Payload payload = objectMapper.readValue("{}", Payload.class);
        assertThat(payload.getValue()).isNull();
    }

    @Test
    void deserialize_successWhenProvidedAsNull() throws IOException {
        Payload payload = objectMapper.readValue("{ \"value\": null }", Payload.class);
        assertThat(payload.getValue()).isEmpty();
    }

    @Test
    void deserialize_successWhenProvidedAsNotNull() throws IOException {
        Payload payload = objectMapper.readValue("{ \"value\": 1 }", Payload.class);
        assertThat(payload.getValue()).hasValue(1);
    }

    @Getter
    @Setter
    private static class Payload {

        @SuppressWarnings("OptionalUsedAsFieldOrParameterType")
        private Optional<Integer> value;

    }

}