package real.al.knowles.ratpack.learning.project;

import com.adaptavist.tm4j.junit.extensions.annotation.SqlFixture;
import com.querydsl.core.Tuple;
import com.querydsl.sql.Configuration;
import com.querydsl.sql.MySQLTemplates;
import com.querydsl.sql.SQLQueryFactory;
import org.flywaydb.core.Flyway;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mariadb.jdbc.MariaDbDataSource;
import org.testcontainers.containers.MySQLContainer;

import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.SQLException;
import java.time.LocalDateTime;
import java.util.Optional;

import static com.adaptavist.tm4j.junit.extensions.annotation.SqlFixture.Phase.SETUP;
import static com.adaptavist.tm4j.junit.extensions.annotation.SqlFixture.Phase.TEARDOWN;
import static org.assertj.core.api.Assertions.assertThat;
import static real.al.knowles.ratpack.learning.schema.QProject.project;


@SqlFixture(phase = SETUP, scripts = "sql/repository/project_repository_setup.sql")
@SqlFixture(phase = TEARDOWN, scripts = "sql/repository/project_repository_teardown.sql")
class ProjectRepositoryTest {

    private static MySQLContainer DATABASE;

    private static SQLQueryFactory dsl;

    private ProjectRepository projectRepository;

    @BeforeAll
    static void setUpBase() throws SQLException {
        DATABASE = new MySQLMariaDbContainer("mysql:5.6");
        DATABASE.start();

        String jdbcUrl = DATABASE.getJdbcUrl();
        String username = DATABASE.getUsername();
        String password = DATABASE.getPassword();

        String url = String.format("%s?user=%s&password=%s", jdbcUrl, username, password);
        MariaDbDataSource dataSource = new MariaDbDataSource(url);
        migrate(dataSource);

        Connection connection = dataSource.getConnection();
        dsl = new SQLQueryFactory(new Configuration(new MySQLTemplates()), () -> connection);
    }

    private static void migrate(DataSource dataSource) {
        Flyway flyway = Flyway.configure().dataSource(dataSource).load();
        flyway.migrate();
    }

    @AfterAll
    static void tearDownClass() {
        DATABASE.stop();
    }

    @BeforeEach
    void setUp() {
        projectRepository = new ProjectRepository(dsl);
    }

    @Test
    void testCreateProject() {
        ProjectRepresentation input = new ProjectRepresentation();
        input.setId(1231L);
        input.setExternalId("KEY");
        input.setCreatedOn(LocalDateTime.now());
        input.setUpdatedOn(LocalDateTime.now());

        Long id = projectRepository.createProject(input);

        assertThat(id).isGreaterThan(0);

        Tuple tuple = dsl.select(project.id, project.externalId)
                .from(project)
                .where(project.id.eq(id))
                .fetchOne();

        assertThat(tuple.get(project.id)).isEqualTo(id);
        assertThat(tuple.get(project.externalId)).isEqualTo("KEY");
    }

    @Test
    @SuppressWarnings("OptionalGetWithoutIsPresent")
    void testGetProject() {
        Optional<ProjectRepresentation> project = projectRepository.getProject(1L);
        assertThat(project).isPresent();
        assertThat(project.get().getId()).isEqualTo(1L);
    }

    private static final class MySQLMariaDbContainer extends MySQLContainer<MySQLMariaDbContainer> {

        MySQLMariaDbContainer(String dockerImageName) {
            super(dockerImageName);
        }

        @Override
        public String getDriverClassName() {
            return "org.mariadb.jdbc.Driver";
        }

    }

}
