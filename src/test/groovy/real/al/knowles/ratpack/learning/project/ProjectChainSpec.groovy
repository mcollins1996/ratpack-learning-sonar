package real.al.knowles.ratpack.learning.project

import com.adaptavist.tm4j.junit.extensions.annotation.SqlFixture
import groovy.json.JsonSlurper
import okhttp3.MediaType
import okhttp3.OkHttpClient
import okhttp3.Request
import okhttp3.RequestBody
import okhttp3.Response
import org.junit.Test
import ratpack.test.MainClassApplicationUnderTest
import real.al.knowles.ratpack.learning.Main
import spock.lang.AutoCleanup
import spock.lang.Shared
import spock.lang.Specification

import java.time.LocalDateTime

import static com.adaptavist.tm4j.junit.extensions.annotation.SqlFixture.Phase.SETUP
import static com.adaptavist.tm4j.junit.extensions.annotation.SqlFixture.Phase.TEARDOWN
import static java.time.LocalDateTime.now
import static org.assertj.core.api.Assertions.assertThat

@SqlFixture(phase = SETUP, scripts = "sql/chain/project_chain_setup.sql")
@SqlFixture(phase = TEARDOWN, scripts = "sql/chain/project_chain_teardown.sql")
class ProjectChainSpec extends Specification {

    private static final MediaType MEDIA_TYPE = MediaType.parse("application/json; charset=utf-8")

    @Shared
    @AutoCleanup
    private MainClassApplicationUnderTest applicationUnderTest

    @Shared
    private String projectUrl

    @Shared
    private OkHttpClient okHttpClient

    void setupSpec() {
        System.setProperty('profile', 'docker')
        applicationUnderTest = new MainClassApplicationUnderTest(Main.class)
        projectUrl = "${applicationUnderTest.address.toString()}project"
        okHttpClient = new OkHttpClient()
    }

    @Test
    def 'POST project'() {
        when: 'posting project'
        LocalDateTime executionTime = now()
        Response response =
                okHttpClient.newCall(new Request.Builder()
                        .url(projectUrl)
                        .post(RequestBody.create(MEDIA_TYPE, '{ \"externalId\": \"example\" }'))
                        .build())
                        .execute()

        then: 'request succeeds'
        response.code() == 200

        and: 'valid project has been created'
        def responseBody = new JsonSlurper().parse(response.body().bytes())
        responseBody.id > 0
        responseBody.externalId == 'example'
        assertThat(LocalDateTime.parse((String) responseBody.createdOn)).isAfter(executionTime)
        assertThat(LocalDateTime.parse((String) responseBody.updatedOn)).isAfter(executionTime)
    }

    @Test
    def 'GET project'() {

    }

}
