package real.al.knowles.ratpack.learning.project;

import com.google.inject.Inject;
import ratpack.func.Action;
import ratpack.handling.Chain;
import ratpack.handling.Context;
import ratpack.jackson.Jackson;

import static real.al.knowles.ratpack.learning.chain.ChainUtils.pathTokenAsLong;

public class ProjectChain implements Action<Chain> {

    private final ProjectService projectService;

    @Inject
    public ProjectChain(ProjectService projectService) {
        this.projectService = projectService;
    }

    @Override
    public void execute(Chain chain) throws Exception {
        chain.post(context ->
            context.parse(ProjectRepresentation.class)
                    .flatMap(projectService::createProject)
                    .map(Jackson::json)
                    .then(context::render)
        )
        .prefix(":id", this::executeId);
    }

    public void executeId(Chain chain) {
        chain.get(this::getProject)
            .delete(this::deleteProject);
    }

    private void getProject(Context ctx) {
        Long id = pathTokenAsLong(ctx, "id");
        projectService.getProject(id)
                .map(Jackson::json)
                .then(ctx::render);
    }

    private void deleteProject(Context ctx) {
        //Delete project
    }



}
