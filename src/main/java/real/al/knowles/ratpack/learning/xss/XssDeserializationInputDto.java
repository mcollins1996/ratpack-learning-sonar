package real.al.knowles.ratpack.learning.xss;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;

import java.util.List;

import static real.al.knowles.ratpack.learning.xss.XssDeserializationMode.BDD;
import static real.al.knowles.ratpack.learning.xss.XssDeserializationMode.HTML;

@Getter
class XssDeserializationInputDto {

    private final String plainInput;

    @XssDeserialization(HTML)
    private final String htmlInput;

    @XssDeserialization(BDD)
    private final String bddInput;

    private final List<XssDeserializationInputDto> listedInput;

    private final XssDeserializationInputDto nestedInput;

    @JsonCreator
    XssDeserializationInputDto(@JsonProperty("plainInput") String plainInput,
                               @JsonProperty("htmlInput") String htmlInput,
                               @JsonProperty("bddInput") String bddInput,
                               @JsonProperty("listedInput") List<XssDeserializationInputDto> listedInput,
                               @JsonProperty("nestedInput") XssDeserializationInputDto nestedInput) {
        this.plainInput = plainInput;
        this.htmlInput = htmlInput;
        this.bddInput = bddInput;
        this.listedInput = listedInput;
        this.nestedInput = nestedInput;
    }

}
