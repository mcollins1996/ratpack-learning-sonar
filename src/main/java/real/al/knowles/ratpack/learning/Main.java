package real.al.knowles.ratpack.learning;

import com.google.common.base.Joiner;
import ratpack.config.ConfigData;
import ratpack.func.Action;
import real.al.knowles.ratpack.learning.database.DatabaseModule;
import real.al.knowles.ratpack.learning.database.DatabaseProperties;
import real.al.knowles.ratpack.learning.jackson.JacksonModule;
import real.al.knowles.ratpack.learning.project.ProjectChain;
import real.al.knowles.ratpack.learning.project.ProjectModule;

import static ratpack.guice.Guice.registry;
import static ratpack.server.RatpackServer.start;
import static real.al.knowles.ratpack.learning.database.DatabaseProperties.loadDatabaseProperties;

public class Main {

    public static void main(String[] args) throws Exception {
        String profile = System.getProperty("profile");
        String databasePropertiesFilePath = getPropertiesFilePath("db", profile, "database");

        ConfigData configData =
                ConfigData.builder()
                        .props(Main.class.getClassLoader().getResource(databasePropertiesFilePath))
                        .build();

        Action<DatabaseProperties> databaseProperties = loadDatabaseProperties(configData);

        start(server -> server
                .registry(registry(binding -> binding
                        .module(JacksonModule.class)
                        .module(DatabaseModule.class, databaseProperties)
                        .module(ProjectModule.class)))
                .handlers(chain -> chain
                        .get(context -> context.render("Welcome to Adaptavist Ratpack Learning"))
                        .prefix("project", ProjectChain.class)));
    }

    @SuppressWarnings("SameParameterValue")
    private static String getPropertiesFilePath(String directory, String profile, String domain) {
        return String.format("%s/%s.properties", directory, Joiner.on("-").skipNulls().join(domain, profile));
    }

}
