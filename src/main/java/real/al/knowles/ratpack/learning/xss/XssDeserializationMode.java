package real.al.knowles.ratpack.learning.xss;

import com.fasterxml.jackson.databind.JsonDeserializer;
import lombok.Getter;

@Getter
public enum XssDeserializationMode {

    PLAIN(new XssDeserializerPlain()),
    HTML(new XssDeserializerHtml()),
    BDD(new XssDeserializerBdd());

    private JsonDeserializer<String> deserializer;

    XssDeserializationMode(JsonDeserializer<String> deserializer) {
        this.deserializer = deserializer;
    }

}
