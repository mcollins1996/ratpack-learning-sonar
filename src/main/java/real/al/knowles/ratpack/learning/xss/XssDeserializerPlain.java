package real.al.knowles.ratpack.learning.xss;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;

class XssDeserializerPlain extends JsonDeserializer<String> {

    @Override
    public String deserialize(JsonParser parser, DeserializationContext context) {
        System.out.print("Deserializing PLAIN\n");
        return "plainInput";
    }

}
