package real.al.knowles.ratpack.learning.xss;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.BeanProperty;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;
import com.fasterxml.jackson.databind.deser.ContextualDeserializer;

import java.io.IOException;

import static real.al.knowles.ratpack.learning.xss.XssDeserializationMode.PLAIN;

public class XssDeserializer extends JsonDeserializer<String> implements ContextualDeserializer {

    @Override
    public JsonDeserializer<String> createContextual(DeserializationContext context, BeanProperty property) {
        XssDeserialization xssDeserialization = property.getAnnotation(XssDeserialization.class);
        if (xssDeserialization == null) {
            return PLAIN.getDeserializer();
        }

        XssDeserializationMode mode = xssDeserialization.value();
        return mode.getDeserializer();
    }

    @Override
    public String deserialize(JsonParser parser, DeserializationContext context) throws IOException {
        return parser.getText();
    }

}
