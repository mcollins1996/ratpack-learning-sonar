package real.al.knowles.ratpack.learning.project;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.RequiredArgsConstructor;

@Getter
@RequiredArgsConstructor
@EqualsAndHashCode(exclude = "otherProtectedString")
public class ImmutableExample {

    private final String protectedString;

    private final String otherProtectedString;

}
