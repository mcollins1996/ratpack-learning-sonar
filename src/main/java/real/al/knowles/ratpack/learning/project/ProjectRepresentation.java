package real.al.knowles.ratpack.learning.project;

import lombok.*;

import javax.validation.constraints.NotNull;
import java.sql.Timestamp;
import java.time.LocalDateTime;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(onlyExplicitlyIncluded = true)
public class ProjectRepresentation {

    private Long id;

    @NotNull
    private String externalId;

    private LocalDateTime createdOn;

    private LocalDateTime updatedOn;

    @SuppressWarnings("unused")
    public ProjectRepresentation(Long id, String externalId, Timestamp createdOn, Timestamp updatedOn) {
        this.id = id;
        this.externalId = externalId;
        this.createdOn = createdOn.toLocalDateTime();
        this.updatedOn = updatedOn.toLocalDateTime();
    }
}
